﻿using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using System;

namespace may.Droid
{

	public class HomeScreenAdapter : BaseAdapter<exchangerateExchangerate> {
		List <exchangerateExchangerate>  items;
		Activity context;
		public HomeScreenAdapter(Activity context, List <exchangerateExchangerate>  items)
			: base()
		{
			this.context = context;
			this.items = items;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override exchangerateExchangerate this[int position]
		{
			get { return items[position]; }

		}
		public override int Count
		{
			get { return   items.Count; }
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = items[position];
			View view = convertView;
			if (view == null) 
				view = context.LayoutInflater.Inflate(Resource.Layout.UnitRow, null);
			view.FindViewById<TextView> (Resource.Id.ccy_name).Text = item.ccy;
			var temp = item.buy / item.unit / 10000;
			view.FindViewById<TextView>(Resource.Id.unit).Text = temp.ToString();
			view.FindViewById<TextView>(Resource.Id.date).Text=item.date;
			//лучше найти все один раз и пользоваться
			//так понятнее
			//и может быть немного быстрее
			var uaMoneyTextEdit = view.FindViewById<EditText> (Resource.Id.UaMoney);
			var ccMoneyTextEdit = view.FindViewById<EditText> (Resource.Id.CcvMoney);
			//KeyPress срабатывает по нажатию кнопки и текста еще нету
			ccMoneyTextEdit.TextChanged+= (sender, e) =>
			{
				if(sender !=view.FindFocus())
					return;


				//отлавливать все подряд ошибки не нужно
				//особенно если ты их никакк не обрабатываешь
				decimal val;
				//просто проверь, можешь ли ты текст преобразовать в число
				if(decimal.TryParse(ccMoneyTextEdit.Text, out val)){
					var course = item.buy / item.unit / 10000;
					decimal output = Math.Round(val*course,2);
					uaMoneyTextEdit.Text= output.ToString();
				}else{
					
					uaMoneyTextEdit.Text="";
				}
			};

			//перепиши вторую функцию и подумай как можно избежать зацикливани
			uaMoneyTextEdit.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) => {
				if(sender !=view.FindFocus())
					return;

				decimal val;
				if(decimal.TryParse(uaMoneyTextEdit.Text, out val)){
					var course = item.buy / item.unit / 10000;
					decimal output = Math.Round(val/course,2);
					ccMoneyTextEdit.Text= output.ToString();
				}else{

					ccMoneyTextEdit.Text="";
				}
			};

				return view;
		}
	}

}

