﻿using System;
using Java.Net;
using Javax.Xml.Parsers;
using Org.Xml.Sax;
using System.Threading.Tasks;
using System.Json;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Xml;

namespace may.Droid
{
	public class request
	{
		public request ()
		{
		}

		public string SendRequest()
		{
			WebRequest request = WebRequest.Create (
				"https://privat24.privatbank.ua/p24/accountorder?oper=prp&PUREXML&apicour&country=ua");
			request.Credentials = CredentialCache.DefaultCredentials;
			WebResponse response = request.GetResponse ();
			Console.WriteLine (((HttpWebResponse)response).StatusDescription);
			Stream dataStream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (dataStream);
			string responseFromServer = reader.ReadToEnd ();
			Console.WriteLine (responseFromServer);
			reader.Close ();
			response.Close ();
			return responseFromServer;

		}

		public exchangerateExchangerate [] DayCourse(string responseFromServer)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(exchangerate));
			StringReader StR = new StringReader (responseFromServer);
			XmlReader SR =XmlReader.Create(StR);
			var po = (exchangerate) serializer.Deserialize(SR);

			return po.exchangerate1;
		}


	}
}

