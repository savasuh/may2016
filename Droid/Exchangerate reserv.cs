using System;

namespace may.Droid
{
		
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
	[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
	public partial class exchangerate {

		private exchangerateExchangerate[] exchangerate1Field;


		[System.Xml.Serialization.XmlElementAttribute("exchangerate")]
		public exchangerateExchangerate[] exchangerate1 {
			get {
				return this.exchangerate1Field;
			}
			set {
				this.exchangerate1Field = value;
			}
		}
	}

	/// <remarks/>
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
	public partial class exchangerateExchangerate {

		string ccyField;
		string ccy_name_ruField;
		string ccy_name_uaField;
		string ccy_name_enField;
		string base_ccyField;
		uint buyField;
		decimal unitField;
		 string dateField;
		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ccy {
			get {
				return this.ccyField;
			}
			set {
				this.ccyField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ccy_name_ru {
			get {
				return this.ccy_name_ruField;
			}
			set {
				this.ccy_name_ruField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ccy_name_ua {
			get {
				return this.ccy_name_uaField;
			}
			set {
				this.ccy_name_uaField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ccy_name_en {
			get {
				return this.ccy_name_enField;
			}
			set {
				this.ccy_name_enField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string base_ccy {
			get {
				return this.base_ccyField;
			}
			set {
				this.base_ccyField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public uint buy {
			get {
				return this.buyField;
			}
			set {
				this.buyField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public decimal unit {
			get {
				return this.unitField;
			}
			set {
				this.unitField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string date {
			get {
				return this.dateField;
			}
			set {
				this.dateField = value;
			}
		}
	}

//
//	p

























}

